# IMSS: In Memory Storage Service

This repo is the result of a tech assessment. The goal is to demonstrate a simple server/client program that stores messages in memory

## Usage

By default, without specific arguments, the code will run in "demo" mode, which makes the client loop and perform a new send every 30s.

#### Cargo local:
```
cargo run --bin server
cargo run --bin client
```

#### Docker local:
```
./build.sh
```

#### Docker compose from Gitlab registry:
Images are built within my k8s cluster using gitlab-runner.

```
podman-compose up
```


## Environment variables
- `IMSS_SERVER_ADDR` server address, default: `127.0.0.1` for client to reach local server
- `IMSS_SERVER_PORT` server port, default `10067`

## Further considerations and improvements
Some things that could be done to further improve this project

- sql DB to store messages, rather than in memory
- REST API for client, to actually use the client other than CLI
