#! /bin/bash

podman build --file docker/Dockerfile_client --tag imss_client .
podman build --file docker/Dockerfile_server --tag imss_server .
