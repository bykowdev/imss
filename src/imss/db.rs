// use std::sync::Mutex;

// // Should be changed for SQL DB
// #[derive(Debug, Default)]
// pub struct ServerDB {
//     pub inner: Mutex<Vec<Entries>>,
// }

// #[derive(Debug, PartialEq, Clone)]
// pub struct Entries {
//     pub tenant: String,
//     pub content: String,
// }

// impl ServerDB {
//     fn add(&self, tenant: String, content: String) -> (u64, bool) {
//         let mut db = self.inner.lock().unwrap();

//         match db.iter().position(|entry| {
//             entry.eq(&Entries {
//                 tenant: tenant.clone(),
//                 content: content.clone(),
//             })
//         }) {
//             Some(index) => (index as u64, false),
//             None => {
//                 db.push(Entries {
//                     tenant: tenant.clone(),
//                     content: content.clone(),
//                 });
//                 ((db.len() - 1) as u64, true) // minus 1 to get correct index..
//             }
//         }
//     }
// }
