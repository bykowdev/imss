use clap::Parser;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
pub struct Cli {
    #[command(subcommand)]
    pub command: Option<Command>,
}

#[derive(clap::Subcommand)]
pub enum Command {
    /// Register an user
    Send {
        /// Tenant
        #[arg(short, long)]
        tenant: String,

        /// Content
        #[arg(short, long)]
        content: String,
    },
}
