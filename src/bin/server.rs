use std::error::Error;
use std::{env, sync::Arc};

use env_logger::Env;
use imss_rust::imss::imss_proto::{
    imss_server::{Imss, ImssServer},
    SendRequest, SendResponse,
};
use tokio::sync::Mutex;
use tonic::{transport::Server, Request, Response, Status};

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    let port: String = match env::var_os("IMSS_SERVER_PORT") {
        Some(p) => p.into_string().unwrap(),
        None => "10067".to_string(), // default parameter
    };

    log::info!("Starting IMSS server");
    log::info!("Port: {}", port);

    let server_db: ServerDB = ServerDB {
        inner: Arc::new(Mutex::new(Vec::new())),
    };

    Server::builder()
        .add_service(ImssServer::new(server_db))
        .serve(
            format!("0.0.0.0:{}", port)
                .parse()
                .expect("Could not parse port"),
        )
        .await?;

    Ok(())
}

// Should be changed for SQL DB or else
#[derive(Debug, Default)]
pub struct ServerDB {
    pub inner: Arc<Mutex<Vec<Entries>>>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Entries {
    pub tenant: String,
    pub content: String,
}

impl ServerDB {
    async fn add(&self, tenant: &str, content: &str) -> (u64, bool) {
        let mut db = self.inner.lock().await;
        let e = Entries {
            tenant: tenant.to_owned(),
            content: content.to_owned(),
        };

        // O(n)
        match db.iter().position(|entry| entry.eq(&e)) {
            Some(index) => (index as u64, false),
            None => {
                db.push(e);
                ((db.len() - 1) as u64, true) // minus 1 to get correct index..
            }
        }
    }
}

#[tonic::async_trait]
impl Imss for ServerDB {
    async fn send(&self, request: Request<SendRequest>) -> Result<Response<SendResponse>, Status> {
        let req = request.into_inner();
        log::info!("New message: {:?}", &req);

        let (id, is_new) = self.add(&req.tenant, &req.content).await;
        // log::info!("Response is: {:?}", &req);

        Ok(Response::new(SendResponse {
            timestamp: format!("{:?}", chrono::offset::Local::now()),
            id,
            is_new,
        }))
    }
}
