use std::{env, thread, time};

use clap::Parser;
use env_logger::Env;
use rand::Rng;
use tonic::transport::Channel;

use imss_rust::imss::{
    cli::*,
    imss_proto::{imss_client::ImssClient, SendRequest},
};

#[tokio::main]
async fn main() {
    let cli = Cli::parse();
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    let addr: String = match env::var_os("IMSS_SERVER_ADDR") {
        Some(p) => p.into_string().unwrap(),
        None => "127.0.0.1".to_string(), // default parameter
    };

    let port: String = match env::var_os("IMSS_SERVER_PORT") {
        Some(p) => p.into_string().unwrap(),
        None => "10067".to_string(), // default parameter
    };

    log::info!("Starting IMSS client");
    log::info!("Attempting to connect to: http://{}:{}", addr, port);

    let mut client: Client = Client::new(format!("http://{}:{}", addr, port)).await;

    match &cli.command {
        Some(Command::Send { tenant, content }) => {
            client.send(tenant, content).await;
        }
        None => {
            log::warn!("No command provided, assuming demo mode, non interactive");

            let known_tenant = String::from("Lawrence");
            let known_content = String::from("abcdefg");

            // it's a demo huh ?
            client.send(&known_tenant, &known_content).await;

            let mut rng = rand::thread_rng();
            loop {
                match rng.gen_range(0..5) {
                    0 => {
                        log::warn!("Bingo ! Sending known tenant and content.");
                        client.send(&known_tenant, &known_content).await
                    }
                    _ => {
                        let tenant = random_string::generate(6, random_string::charsets::NUMERIC);
                        let content =
                            random_string::generate(8, random_string::charsets::ALPHANUMERIC);

                        // Leaking for demonstration
                        log::info!("Tenant: {}", tenant);
                        log::info!("Content: {}", content);

                        client.send(&tenant, &content).await;
                    }
                }

                log::info!("Sleeping 30s before next run");
                thread::sleep(time::Duration::from_secs(30));
            }
        }
    }
}

#[derive(Debug)]
pub struct Client {
    pub inner: ImssClient<Channel>,
}

impl Client {
    async fn new(address: String) -> Client {
        let connexion = ImssClient::connect(address).await;
        match connexion {
            Ok(c) => {
                log::info!("Client connected");

                Client { inner: c }
            }
            Err(e) => {
                log::error!("Could not connect to imss server with error: {:?}", e);
                panic!(); // Client should not run if it can't connect to server
            }
        }
    }

    async fn send(&mut self, tenant: &String, content: &String) {
        let send_request = SendRequest {
            tenant: tenant.to_string(),
            content: content.to_string(),
        };

        match self.inner.send(send_request).await {
            Ok(r) => log::info!("Send response: {:?}", r),
            Err(e) => log::error!("Send failed with error: {:?}", e),
        }
    }
}
